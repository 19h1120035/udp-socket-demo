﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace Client
{
    class Program
    {
        // Chương trình phía Client
        static void Main(string[] args)
        {
            // 1,2.  Xác định địa chỉ của server, tạo 1 socket
            IPEndPoint ipep = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 6969);
            Socket client = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            // Tạo 1 EndPoint gửi/nhận dữ liệu
            EndPoint remote = (EndPoint)ipep;
            // 3. Gửi nhận dữ liệu theo giao thức
            byte[] data = new byte[1024];
            data = Encoding.ASCII.GetBytes("Hello server...");
            client.SendTo(data, remote);
            data = new byte[1024];
            int receive = client.ReceiveFrom(data, ref remote);
            String s = Encoding.ASCII.GetString(data, 0, receive);
            Console.WriteLine("Server: " + s);
            while (true)
            {
                data = new byte[1024];
                s = Console.ReadLine();
                data = Encoding.ASCII.GetBytes(s);
                client.SendTo(data, remote);
                if (s.ToUpper().Equals("QUIT")) break;
                data = new byte[1024];
                receive = client.ReceiveFrom(data, ref remote);
                s = Encoding.ASCII.GetString(data, 0, receive);
                Console.WriteLine("Server: " + s);
            }
            client.Close();
        }
    }
}
