﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace Server
{
    class Program
    {
        // Chương trình phía server
        static void Main(string[] args)
        {
            // 1. Tạo Socket
            IPEndPoint ipep = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 6969);
            Socket server = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

            //2. Liên kết socket IPEndPoint cục bộ
            server.Bind(ipep);
            Console.WriteLine("Waiting to connect...");
            // Tạo một EndPoint gửi nhận dữ liệu 
            IPEndPoint remoteEp = new IPEndPoint(IPAddress.Any, 0);
            EndPoint remote = (EndPoint)remoteEp;
            // Gửi nhận dữ liệu theo giao thức tạo 
            byte[] data = new byte[1024];
            int receive = server.ReceiveFrom(data, ref remote);
            String s = Encoding.ASCII.GetString(data, 0, receive);
            Console.WriteLine("Client: " + s);
            data = new byte[1024];
            data = Encoding.ASCII.GetBytes("Welcome to server");
            server.SendTo(data, remote);
            while (true)
            {
                data = new byte[1024];
                receive = server.ReceiveFrom(data, ref remote);
                s = Encoding.ASCII.GetString(data, 0, receive);
                if (s.ToUpper().Equals("QUIT")) break;

                Console.WriteLine("Client: " + s);
                data = new byte[1024];
                data = Encoding.ASCII.GetBytes(s.ToUpper());
                server.SendTo(data, remote);
            }

            // 4. Đóng Socket
            server.Close();
        }
    }
}
